# build environment
FROM node:13.12.0-alpine as build
# set the directory we want to run the next commands for
WORKDIR /app

ENV PATH /app/node_modules/.bin:$PATH
COPY package.json ./
COPY yarn.lock ./

# set env vars
ENV NODE_ENV=production

#set proxy for yarn
RUN yarn config set proxy http://10.0.0.10:80 
RUN yarn config set https-proxy http://10.0.0.10:80 

# install the dependencies, can be commented out if you're running the same node version
RUN yarn install
COPY . ./
# build the client
RUN yarn build

# production environment
FROM nginx:stable-alpine
RUN rm -rf /etc/nginx/conf.d
COPY conf /etc/nginx
COPY --from=build /app/build /usr/share/nginx/html
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]

