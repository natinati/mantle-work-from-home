import React, { useState, useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import { Tabs, Tab } from "@material-ui/core";
import { Link, useLocation, useRouteMatch, Redirect } from "react-router-dom";
const useStyles = makeStyles(() => ({
  navBar: {
    flexGrow: 1,
    direction: "rtl",
    background: "transparent"
  }
}));

const NavBar = () => {
  const classes = useStyles();
  const location = useLocation();
  const match = useRouteMatch("/group/:groupId");
  const [value, setValue] = useState(
    location.pathname.endsWith("/manage-data") ? 0 : 1
  );
  useEffect(() => {
    setValue(location.pathname.endsWith("/manage-data") ? 0 : 1);
  }, [location.pathname, setValue]);
  const handleChange = (_, newValue) => {
    setValue(newValue);
  };
  return match ? (
    <div>
      <Tabs value={value} className={classes.navBar} onChange={handleChange}>
        <Tab
          label="קבוצות ומשתמשים"
          component={Link}
          disableRipple={true}
          to={{
            pathname: `/group/${match.params.groupId}/manage-data`
          }}
        />
        <Tab
          label="הזנת משתמשים"
          component={Link}
          disableRipple={true}
          to={{
            pathname: `/group/${match.params.groupId}/bulk-insert`
          }}
        />
      </Tabs>
    </div>
  ) : (
    <Redirect to="/" />
  );
};

export default NavBar;
