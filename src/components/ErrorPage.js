import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import { Grid, Typography, Button } from "@material-ui/core";
import { useLocation } from "react-router-dom";
const useStyles = makeStyles(() => ({
  headerIMG: {
    width: "70px",
    height: "70px",
    marginBottom: "-8%"
  },
  bodyText: {
    marginTop: "2%"
  },
  directionLTR: {
    direction: "ltr"
  }
}));
const ErrorPage = () => {
  const location = useLocation();
  const classes = useStyles();
  return (
    <Grid container direction="column" spacing={10}>
      <Grid container item direction="column">
        <Grid item>
          <Typography variant="h5">
            <img className={classes.headerIMG} src="logo.png" alt="מנטל" />|
            ברוכים הבאים למנטל!
          </Typography>
        </Grid>
        <Grid item>
          <Typography variant="h6" className={classes.bodyText}>
            מערכת זו מאפשרת פתיחה תשתיתית, ניהול מבנה יחידה ובקרה על קבוצות
            ומשתמשים ביחידה.
          </Typography>
        </Grid>
      </Grid>
      <Grid container item alignItems="center" direction="column" spacing={6}>
        <Grid item>
          <Typography variant="h4">אירעה שגיאה!</Typography>
        </Grid>
        <Grid container direction="column" spacing={4}>
          <Grid item>
            <Typography variant="h6">
              בדקו האם יש לכם הרשאות מתאימות ולחצו על נסה שנית.
            </Typography>
          </Grid>
          {location.state.error && (
            <Grid container item direction="row" spacing={2}>
              <Grid item>
                <Typography variant="h6">
                  במידה ועדיין יש שגיאה פנו אל צוות הפיתוח עם השגיאה הבאה:
                </Typography>
              </Grid>
              <Grid item className={classes.directionLTR}>
                <Typography variant="h6">{`${location.state.error}`}</Typography>
              </Grid>
            </Grid>
          )}
        </Grid>
        <Grid item>
          <Button
            variant="contained"
            color="secondary"
            style={{ fontSize: "16px" }}
            onClick={() => {
              window.location.href = "/";
            }}
          >
            נסה שנית
          </Button>
        </Grid>
      </Grid>
    </Grid>
  );
};

export default ErrorPage;
