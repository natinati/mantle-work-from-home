import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import { Grid, Typography } from "@material-ui/core";
const useStyles = makeStyles(() => ({
  headerIMG: {
    width: "70px",
    height: "70px",
    marginBottom: "-8%"
  },
  bodyText: {
    marginTop: "2%"
  }
}));
const HomePage = () => {
  const classes = useStyles();
  return (
    <Grid container direction="column">
      <Grid item>
        <Typography variant="h5">
          <img className={classes.headerIMG} src="logo.png" alt="מנטל" />|
          ברוכים הבאים למנטל!
        </Typography>
      </Grid>
      <Grid item>
        <Typography variant="h6" className={classes.bodyText}>
          מערכת זו מאפשרת פתיחה תשתיתית, ניהול מבנה יחידה ובקרה על קבוצות
          ומשתמשים ביחידה.
        </Typography>
      </Grid>
      <Grid item>
        <Typography variant="body1" className={classes.bodyText}>
          נא לבחור קבוצה כדי להמשיך.
        </Typography>
      </Grid>
    </Grid>
  );
};

export default HomePage;
