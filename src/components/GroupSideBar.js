import React, { memo, useState, useRef, useEffect } from "react";
import { useSpring, a, animated } from "react-spring";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import KeyboardArrowLeftIcon from "@material-ui/icons/KeyboardArrowLeft";
import ResizeObserver from "resize-observer-polyfill";
import styled from "styled-components";
import { Link, useRouteMatch, Redirect } from "react-router-dom";
import AllGroups from "../graphql/AllGroups";
import { useQuery } from "@apollo/react-hooks";
import * as R from "ramda";
import RemoveIcon from "@material-ui/icons/Remove";
import { CircularProgress, Grid } from "@material-ui/core";

const GroupSideBar = () => {
  const groups = useQuery(AllGroups);
  return groups.loading ? (
    <Grid container justify="center" alignItems="center">
      <CircularProgress color="secondary" />
    </Grid>
  ) : groups.error ? (
    <Grid>
      <Redirect to={{ pathname: "/error", state: { error: groups.error } }} />
    </Grid>
  ) : (
    <Node group={{ name: 'בסמ"ח', id: 1 }} defaultOpen>
      {groups.data.madors.map(mador => (
        <Node group={mador} key={mador.id} defaultOpen>
          {mador.megamot.map(megama => (
            <Node group={megama} key={megama.id}>
              {megama.cycles.length
                ? megama.cycles.map(cycle => (
                    <Node group={cycle} key={cycle.id}>
                      {cycle.courses.map(course => (
                        <Node group={course} key={course.id}>
                          {course.departments.map(department => (
                            <Node group={department} key={department.id}></Node>
                          ))}
                          {course.teams.map(team => (
                            <Node group={team} key={team.id}></Node>
                          ))}
                        </Node>
                      ))}
                    </Node>
                  ))
                : megama.courses.map(course => (
                    <Node group={course} key={course.id}>
                      {course.departments.map(department => (
                        <Node group={department} key={department.id}></Node>
                      ))}
                      {course.teams.map(team => (
                        <Node group={team} key={team.id}></Node>
                      ))}
                    </Node>
                  ))}
            </Node>
          ))}
        </Node>
      ))}
    </Node>
  );
};

const Node = memo(({ children, style, defaultOpen = false, group }) => {
  const match = useRouteMatch("/group/:groupId");
  useEffect(() => {
    const extractChildren = group => {
      return group.props.children
        ? R.unnest(group.props.children)
            .map(child => extractChildren(child))
            .concat(group.props.group.id)
        : group.props.group.id;
    };
    const childrenIds = children
      ? R.unnest(children).map(child => extractChildren(child))
      : [];
    setOpen(
      defaultOpen ||
        (match && R.flatten(childrenIds).includes(Number(match.params.groupId)))
    );
    // eslint-disable-next-line
  }, []);
  const [isOpen, setOpen] = useState(defaultOpen);
  const previous = usePrevious(isOpen);
  const [bind, { height: viewHeight }] = useMeasure();
  const { height, opacity, transform } = useSpring({
    from: { height: 0, opacity: 0, transform: "translate3d(20px,0,0)" },
    to: {
      height: isOpen ? viewHeight : 0,
      opacity: isOpen ? 1 : 0,
      transform: `translate3d(${isOpen ? 0 : 20}px,0,0)`
    }
  });
  const Icon =
    children && R.flatten(children).length
      ? isOpen
        ? ExpandMoreIcon
        : KeyboardArrowLeftIcon
      : RemoveIcon;
  return (
    <Frame>
      <Icon
        style={{
          ...toggle,
          opacity: children && R.flatten(children).length ? 1 : 0.3
        }}
        onClick={() => {
          setOpen(!isOpen);
        }}
      />
      <Title
        style={style}
        shouldbebold={
          match ? (Number(match.params.groupId) === group.id ? 1 : 0) : 0
        }
        to={{
          pathname: `/group/${group.id}/manage-data`
        }}
      >
        {group.name}
      </Title>
      <Content
        style={{
          opacity,
          height: isOpen && previous === isOpen ? "auto" : height
        }}
      >
        <a.div style={{ transform }} {...bind} children={children} />
      </Content>
    </Frame>
  );
});

const Frame = styled("div")`
  position: relative;
  text-overflow: ellipsis;
  white-space: nowrap;
  overflow-x: hidden;
  vertical-align: middle;
  color: #191b21;
  fill: #191b21;
  direction: rtl;
`;

const Title = styled(Link)`
  vertical-align: middle;
  color: #191b21;
  text-decoration: none;
  font-size: 18px;
  ${({ shouldbebold }) =>
    shouldbebold &&
    `
  font-weight: bold;
  text-decoration: underline;
`}
`;

const Content = styled(animated.div)`
  will-change: transform, opacity, height;
  margin-right: 10px;
  padding-right: 10px;
  border-right: 1px dashed #191b21;
  overflow: hidden;
`;

const toggle = {
  width: "1em",
  height: "1em",
  marginLeft: 10,
  cursor: "pointer",
  verticalAlign: "middle"
};

const usePrevious = value => {
  const ref = useRef();
  useEffect(() => void (ref.current = value), [value]);
  return ref.current;
};

const useMeasure = () => {
  const ref = useRef();
  const [bounds, set] = useState({ left: 0, top: 0, width: 0, height: 0 });
  const [ro] = useState(
    () => new ResizeObserver(([entry]) => set(entry.contentRect))
  );
  useEffect(() => {
    if (ref.current) ro.observe(ref.current);
    return () => ro.disconnect();
  }, [ro]);
  return [{ ref }, bounds];
};

export default GroupSideBar;
