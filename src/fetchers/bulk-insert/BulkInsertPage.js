import React, { useState, useRef } from "react";
import {
  Grid,
  Button,
  Radio,
  RadioGroup,
  FormLabel,
  FormControl,
  FormControlLabel,
  Typography,
  CircularProgress
} from "@material-ui/core";
import { useMutation, useQuery } from "@apollo/react-hooks";
import InsertUsers from "../../graphql/InsertUsers";
import UsersAssignedToGroupByAPI from "../../graphql/UsersAssignedToGroupByAPI";
import GroupTypeById from "../../graphql/GroupTypeById";
import { HotTable } from "@handsontable/react";
import "handsontable/dist/handsontable.full.css";
import Handsontable from "handsontable";
import { useParams, useHistory } from "react-router-dom";
import { makeStyles } from "@material-ui/core/styles";
import Swal from "sweetalert2";
import * as R from "ramda";

const useStyles = makeStyles(() => ({
  gridVerticalSpacing: {
    marginTop: "1%"
  },
  directionLTR: {
    direction: "ltr"
  },
  scrollbar: {
    overflowY: "auto"
  }
}));

const BulkInsertPage = () => {
  const { groupId } = useParams();
  const groupTypeQuery = useQuery(GroupTypeById, {
    variables: { groupId: groupId }
  });
  const [studentsData] = useState(
    localStorage.getItem(groupId + "_students_data")
      ? JSON.parse(localStorage.getItem(groupId + "_students_data"))
      : Handsontable.helper.createEmptySpreadsheetData(20, 5)
  );
  const [staffData] = useState(
    localStorage.getItem(groupId + "_staff_data")
      ? JSON.parse(localStorage.getItem(groupId + "_staff_data"))
      : Handsontable.helper.createEmptySpreadsheetData(20, 5)
  );
  const [validate, setValidate] = useState(false);
  const [selectedInsertType, setSelectedInsertType] = useState("staff");
  const hotTableComponent = useRef();
  const handleTableSelectionChange = event => {
    setSelectedInsertType(event.target.value);
  };
  const [InsertUsersMutation, { loading: insertUserLoading }] = useMutation(
    InsertUsers
  );
  const history = useHistory();
  const classes = useStyles();
  const userRoleOptions = {
    מדריך: 1,
    'צפ"ה': 2,
    'מק"ס': 3,
    'מ"מ': 4,
    'רמ"ד': 6,
    "מפקד יחידה": 7
  };
  const staffRoleInsertOptionBasedOnCurrentGroupType = {
    יחידה: ["מפקד יחידה"],
    מדור: ['רמ"ד'],
    מגמה: ['צפ"ה'],
    קורס: ['מק"ס'],
    מחלקה: ["מדריך"],
    צוות: ["מדריך"]
  };
  const inuptValidation = {
    soldierId: /^[0-9]{7}$/,
    names: /^[\u0590-\u05fe ']+$/,
    notEmpty: /^(?!\s*$).+/
  };
  const clearTable = () => {
    selectedInsertType === "staff"
      ? localStorage.removeItem(groupId + "_staff_data")
      : localStorage.removeItem(groupId + "_students_data");
    hotTableComponent.current.hotInstance.clear();
  };
  const saveChanges = async () => {
    await setValidate(true);
    const adptedStudents = studentsData
      .filter(student => !student.includes("") && !student.includes(null))
      .map(student => ({
        soldier_id: Number(student[4]),
        last_name: student[2],
        first_name: student[3],
        gender: student[1],
        users_roles: {
          data: {
            role_id: 5,
            details: { student_number: student[0] },
            assigns: {
              data: {
                group_id: groupId
              }
            }
          }
        }
      }));
    const adptedStaff = staffData
      .filter(staff => !staff.includes("") && !staff.includes(null))
      .map(staff => ({
        soldier_id: Number(staff[4]),
        last_name: staff[2],
        first_name: staff[3],
        gender: staff[1],
        users_roles: {
          data: {
            role_id: userRoleOptions[staff[0]],
            details: {},
            assigns: {
              data: {
                group_id: groupId
              }
            }
          }
        }
      }));
    const changedRows = hotTableComponent.current.props.data
      .map((row, index) => ({
        hasChanged: row.some(
          cell => inuptValidation["notEmpty"].test(cell) && cell !== null
        ),
        index: index
      }))
      .filter(R.prop("hasChanged"))
      .map(R.prop("index"));
    changedRows.length
      ? hotTableComponent.current.hotInstance.validateRows(
          changedRows,
          valid => {
            valid
              ? InsertUsersMutation({
                  variables: {
                    soldiersData:
                      selectedInsertType === "staff"
                        ? adptedStaff
                        : adptedStudents
                  },
                  awaitRefetchQueries: true,
                  refetchQueries: [
                    {
                      query: UsersAssignedToGroupByAPI,
                      variables: { groupId: groupId }
                    }
                  ]
                })
                  .then(() => {
                    Swal.fire({
                      icon: "success",
                      title: "השינויים נשמרו",
                      toast: true,
                      showConfirmButton: false,
                      timer: 1500
                    });
                    setValidate(false);
                    clearTable();
                    history.push(`/group/${groupId}/manage-data`);
                  })
                  .catch(error => {
                    Swal.fire({
                      icon: "error",
                      title: error,
                      toast: true,
                      showConfirmButton: true
                    });
                  })
              : Swal.fire({
                  icon: "error",
                  title: "הוזן מידע שגוי",
                  toast: true,
                  showConfirmButton: false,
                  timer: 1500
                }) && setTimeout(() => setValidate(false), 1500);
          }
        )
      : Swal.fire({
          icon: "info",
          title: "יש להזין מידע",
          toast: true,
          showConfirmButton: false,
          timer: 1500
        }) && setValidate(false);
  };
  return groupTypeQuery.loading ? (
    <Grid container justify="center" alignItems="center">
      <CircularProgress color="secondary" />
    </Grid>
  ) : groupTypeQuery.data.core_groups[0].group_type.type === "מחזור" ? (
    <Grid container>
      <Grid item style={{ marginTop: "2%" }}>
        <Typography variant="h5">
          {"לא ניתן להזין לקבוצה מסוג מחזור."}
        </Typography>
      </Grid>
    </Grid>
  ) : (
    <Grid container direction="row" alignItems="center">
      <Grid item className={classes.gridVerticalSpacing}>
        <FormControl>
          <FormLabel>בחרו סוג הזנה</FormLabel>
          <RadioGroup
            row
            name="insertionType"
            value={selectedInsertType}
            onChange={handleTableSelectionChange}
          >
            <FormControlLabel
              value="staff"
              control={<Radio />}
              label="סגל"
              labelPlacement="end"
            />
            <FormControlLabel
              value="students"
              control={<Radio />}
              label="חניכים"
              labelPlacement="end"
              disabled={
                !["צוות", "מחלקה"].includes(
                  groupTypeQuery.data.core_groups[0].group_type.type
                )
              }
            />
          </RadioGroup>
        </FormControl>
      </Grid>
      <Grid container>
        <Grid item>
          <Typography>
            ניתן להזין מידע ידנית או באמצעות העתקה והדבקה מExcel.
          </Typography>
        </Grid>
      </Grid>
      <Grid container className={classes.gridVerticalSpacing} justify="center">
        <Grid item className={`${classes.directionLTR} ${classes.scrollbar}`}>
          <HotTable
            colHeaders={
              selectedInsertType === "staff"
                ? ["תפקיד", "מין", "שם משפחה", "שם פרטי", "מספר אישי"]
                : ["מספר חניך", "מין", "שם משפחה", "שם פרטי", "מספר אישי"]
            }
            columns={[
              selectedInsertType === "staff"
                ? {
                    type: "dropdown",
                    source:
                      staffRoleInsertOptionBasedOnCurrentGroupType[
                        groupTypeQuery.data.core_groups[0].group_type.type
                      ],
                    allowEmpty: false,
                    invalidCellClassName: validate ? "htInvalid" : ""
                  }
                : {
                    type: "text",
                    validator: inuptValidation["notEmpty"],
                    invalidCellClassName: validate ? "htInvalid" : ""
                  },
              {
                type: "dropdown",
                source: ["זכר", "נקבה"],
                allowEmpty: false,
                invalidCellClassName: validate ? "htInvalid" : ""
              },
              {
                type: "text",
                validator: inuptValidation["names"],
                invalidCellClassName: validate ? "htInvalid" : ""
              },
              {
                type: "text",
                validator: inuptValidation["names"],
                invalidCellClassName: validate ? "htInvalid" : ""
              },
              {
                type: "numeric",
                validator: inuptValidation["soldierId"],
                invalidCellClassName: validate ? "htInvalid" : ""
              }
            ]}
            data={selectedInsertType === "staff" ? staffData : studentsData}
            width="63em"
            height="37em"
            maxCols={5}
            colWidths="200px"
            rowHeights="28"
            className="htRight"
            minSpareRows={1}
            minRows={20}
            ref={hotTableComponent}
            licenseKey="non-commercial-and-evaluation"
            afterChange={() => {
              localStorage.setItem(
                groupId +
                  (selectedInsertType === "staff"
                    ? "_staff_data"
                    : "_students_data"),
                JSON.stringify(
                  selectedInsertType === "staff" ? staffData : studentsData
                )
              );
            }}
          />
        </Grid>
        <Grid
          container
          justify="center"
          spacing={10}
          className={classes.gridVerticalSpacing}
        >
          <Grid item>
            <Button
              variant="contained"
              color="secondary"
              style={{ fontSize: "16px" }}
              onClick={() => saveChanges()}
              disabled={insertUserLoading}
            >
              {`הזן ${selectedInsertType === "staff" ? "סגל" : "חניכים"}`}
            </Button>
          </Grid>
          <Grid item>
            <Button
              variant="contained"
              color="secondary"
              style={{ fontSize: "16px" }}
              onClick={() => clearTable()}
            >
              נקה טבלה
            </Button>
          </Grid>
        </Grid>
      </Grid>
    </Grid>
  );
};

export default BulkInsertPage;
