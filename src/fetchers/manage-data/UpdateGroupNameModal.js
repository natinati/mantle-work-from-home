import React, { useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import {
  Modal,
  Backdrop,
  Fade,
  Button,
  CircularProgress,
  TextField,
  Typography,
  withStyles,
  Grid
} from "@material-ui/core";
import Swal from "sweetalert2";
import { useMutation } from "@apollo/react-hooks";
import AllGroups from "../../graphql/AllGroups";
import EditGroup from "../../graphql/EditGroup";
import GroupPathByAncestorsView from "../../graphql/GroupPathByAncestorsView";

const StyledTextField = withStyles({
  root: {
    "& .MuiInputLabel-formControl": {
      left: "unset"
    }
  }
})(TextField);

const useStyles = makeStyles(theme => ({
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    direction: "rtl"
  },
  paper: {
    backgroundColor: theme.palette.background.paper,
    border: "2px solid #f50057",
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
    borderRadius: "8px",
    width: "fit-content"
  },
  title: {
    fontWeight: "bold",
    textAlign: "center"
  },
  verticalSpacing: {
    marginTop: "7%"
  },
  button: {
    marginTop: "10%",
    marginBottom: "-8%",
    fontSize: "16px",
    width: "20%"
  },
  loadingAnimation: {
    display: "flex",
    justifyContent: "center"
  }
}));

const UpdateGroupNameModal = ({ modalOpen, setModalopen, currentGroup }) => {
  const classes = useStyles();
  const Toast = Swal.mixin({
    toast: true,
    showConfirmButton: false,
    timer: 1500
  });
  const [newGroupName, setNewGroupName] = useState("");
  const [editGroupMutation, { loading: updateLoading }] = useMutation(
    EditGroup
  );
  const handleGroupNameChange = event => {
    setNewGroupName(event.target.value);
  };
  const hangeCloseModalAction = () => {
    setNewGroupName("");
    setModalopen(false);
    Toast.fire({
      icon: "error",
      title: "הפעולה בוטלה"
    });
  };
  const UpdateGroupNameAction = () => {
    if (newGroupName !== "") {
      editGroupMutation({
        variables: {
          groupId: currentGroup,
          NewGroupName: newGroupName
        },
        awaitRefetchQueries: true,
        refetchQueries: [
          { query: AllGroups },
          {
            query: GroupPathByAncestorsView,
            variables: { groupId: currentGroup }
          }
        ]
      })
        .then(() => {
          Toast.fire({
            icon: "success",
            title: "השינויים נשמרו"
          });
          setNewGroupName("");
          setModalopen(false);
        })
        .catch(error => {
          Swal.fire({
            icon: "error",
            title: error,
            toast: true,
            showConfirmButton: true
          });
        });
    }
  };
  return (
    <Modal
      className={classes.modal}
      open={modalOpen}
      onClose={() => hangeCloseModalAction()}
      closeAfterTransition
      BackdropComponent={Backdrop}
      BackdropProps={{
        timeout: 500
      }}
    >
      <Grid
        container
        direction="column"
        justify="center"
        alignItems="center"
        className={classes.paper}
      >
        <Grid item>
          <Typography variant="h4" className={classes.title}>
            {"שינוי שם לקבוצה"}
          </Typography>
        </Grid>
        <Grid item>
          {updateLoading ? (
            <div
              className={`${classes.verticalSpacing} ${classes.loadingAnimation}`}
            >
              <CircularProgress color="secondary" />
            </div>
          ) : (
            <Fade in={modalOpen}>
              <StyledTextField
                style={{ marginTop: "4%" }}
                label="מה שם הקבוצה החדש?"
                onChange={handleGroupNameChange}
                value={newGroupName}
                error={newGroupName === ""}
                helperText={newGroupName === "" ? "הזינו שם לקבוצה" : ""}
              />
            </Fade>
          )}
        </Grid>
        <Grid container direction="row" justify="center" spacing={5}>
          <Grid item>
            <Button
              variant="contained"
              color="secondary"
              className={`${classes.button}`}
              onClick={() => UpdateGroupNameAction()}
            >
              שמור
            </Button>
          </Grid>
          <Grid item>
            <Button
              variant="contained"
              color="secondary"
              className={`${classes.button}`}
              onClick={() => hangeCloseModalAction()}
            >
              ביטול
            </Button>
          </Grid>
        </Grid>
      </Grid>
    </Modal>
  );
};
export default UpdateGroupNameModal;
