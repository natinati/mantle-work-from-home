import React, { useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import {
  Modal,
  Backdrop,
  Fade,
  Button,
  CircularProgress,
  TextField,
  Typography,
  withStyles,
  Grid
} from "@material-ui/core";
import Swal from "sweetalert2";
import { useMutation } from "@apollo/react-hooks";
import UsersAssignedToGroupByAPI from "../../graphql/UsersAssignedToGroupByAPI";
import DeleteUser from "../../graphql/DeleteUser";
import { useParams } from "react-router-dom";

const StyledTextField = withStyles({
  root: {
    "& .MuiInputLabel-formControl": {
      left: "unset"
    }
  }
})(TextField);

const useStyles = makeStyles(theme => ({
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    direction: "rtl"
  },
  paper: {
    backgroundColor: theme.palette.background.paper,
    border: "2px solid #f50057",
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
    borderRadius: "8px",
    width: "fit-content"
  },
  title: {
    fontWeight: "bold",
    textAlign: "center"
  },
  verticalSpacing: {
    marginTop: "3%"
  },
  button: {
    marginTop: "10%",
    marginBottom: "-8%",
    fontSize: "16px",
    width: "20%"
  },
  loadingAnimation: {
    display: "flex",
    justifyContent: "center"
  }
}));

const DeleteUserModal = ({ modalOpen, setModalopen, user }) => {
  const classes = useStyles();
  const Toast = Swal.mixin({
    toast: true,
    showConfirmButton: false,
    timer: 1500
  });
  const { groupId } = useParams();
  const [UserFullName, setUserFullName] = useState("");
  const [deleteUserMutation, { loading: DeleteLoading }] = useMutation(
    DeleteUser
  );
  const handleUserFullNameChange = event => {
    setUserFullName(event.target.value);
  };
  const hangeCloseModalAction = () => {
    setUserFullName("");
    setModalopen(false);
    Toast.fire({
      icon: "error",
      title: "הפעולה בוטלה"
    });
  };
  const DeleteUserAction = () => {
    UserFullName === user.first_name + " " + user.last_name
      ? deleteUserMutation({
          variables: {
            userId: user.user_id
          },
          awaitRefetchQueries: true,
          refetchQueries: [
            {
              query: UsersAssignedToGroupByAPI,
              variables: { groupId: groupId }
            }
          ]
        })
          .then(() => {
            Toast.fire({
              icon: "success",
              title: "המשתמש נמחק"
            });
          })
          .catch(error => {
            setModalopen(false);
            Swal.fire({
              icon: "error",
              title: error,
              toast: true,
              showConfirmButton: true
            });
          })
      : hangeCloseModalAction();
  };
  return (
    <Modal
      className={classes.modal}
      open={modalOpen}
      onClose={() => hangeCloseModalAction()}
      closeAfterTransition
      BackdropComponent={Backdrop}
      BackdropProps={{
        timeout: 500
      }}
    >
      <Grid
        container
        direction="column"
        justify="center"
        alignItems="center"
        className={classes.paper}
      >
        <Grid item>
          <Typography variant="h4" className={classes.title}>
            {"התראת מחיקה"}
          </Typography>
        </Grid>
        <Grid item>
          <Typography variant="body1" className={`${classes.verticalSpacing}`}>
            {`פעולה זו תמחק את המשתמש ואת כל המידע הקשור למשתמש כמו ציונים, שייכויות וכו'.`}
          </Typography>
        </Grid>
        <Grid item>
          <Typography variant="body1">
            {`אם ברצונכם להמשיך למחיקת המשתמש הקלידו את  שמו המלא: ${user.first_name} ${user.last_name}`}
          </Typography>
        </Grid>
        <Grid item>
          {DeleteLoading ? (
            <div
              className={`${classes.verticalSpacing} ${classes.loadingAnimation}`}
            >
              <CircularProgress color="secondary" />
            </div>
          ) : (
            <Fade in={modalOpen}>
              <StyledTextField
                className={`${classes.verticalSpacing}`}
                label="הקלידו את שם המשתמש"
                onChange={handleUserFullNameChange}
                value={UserFullName}
              />
            </Fade>
          )}
        </Grid>
        <Grid container direction="row" justify="center" spacing={5}>
          <Grid item>
            <Button
              variant="contained"
              color="secondary"
              className={`${classes.button}`}
              onClick={() => DeleteUserAction()}
            >
              מחיקה
            </Button>
          </Grid>
          <Grid item>
            <Button
              variant="contained"
              color="secondary"
              className={`${classes.button}`}
              onClick={() => hangeCloseModalAction()}
            >
              ביטול
            </Button>
          </Grid>
        </Grid>
      </Grid>
    </Modal>
  );
};
export default DeleteUserModal;
