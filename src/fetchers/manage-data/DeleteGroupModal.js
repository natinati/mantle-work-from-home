import React, { useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import {
  Modal,
  Backdrop,
  Fade,
  Button,
  CircularProgress,
  TextField,
  Typography,
  withStyles,
  Grid
} from "@material-ui/core";
import Swal from "sweetalert2";
import { useMutation } from "@apollo/react-hooks";
import AllGroups from "../../graphql/AllGroups";
import DeleteGroup from "../../graphql/DeleteGroup";
import { useHistory } from "react-router-dom";

const StyledTextField = withStyles({
  root: {
    "& .MuiInputLabel-formControl": {
      left: "unset"
    }
  }
})(TextField);

const useStyles = makeStyles(theme => ({
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    direction: "rtl"
  },
  paper: {
    backgroundColor: theme.palette.background.paper,
    border: "2px solid #f50057",
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
    borderRadius: "8px",
    width: "fit-content"
  },
  title: {
    fontWeight: "bold",
    textAlign: "center"
  },
  verticalSpacing: {
    marginTop: "3%"
  },
  button: {
    marginTop: "10%",
    marginBottom: "-8%",
    fontSize: "16px",
    width: "20%"
  },
  loadingAnimation: {
    display: "flex",
    justifyContent: "center"
  }
}));

const DeleteGroupModal = ({
  modalOpen,
  setModalopen,
  currentGroup,
  currentGroupName
}) => {
  const classes = useStyles();
  const Toast = Swal.mixin({
    toast: true,
    showConfirmButton: false,
    timer: 1500
  });
  const history = useHistory();
  const [GroupName, setGroupName] = useState("");
  const [deleteGroupMutation, { loading: DeleteLoading }] = useMutation(
    DeleteGroup
  );
  const handleGroupNameChange = event => {
    setGroupName(event.target.value);
  };
  const hangeCloseModalAction = () => {
    setGroupName("");
    setModalopen(false);
    Toast.fire({
      icon: "error",
      title: "הפעולה בוטלה"
    });
  };
  const DeleteGroupAction = () => {
    GroupName === currentGroupName
      ? deleteGroupMutation({
          variables: {
            groupId: currentGroup
          },
          awaitRefetchQueries: true,
          refetchQueries: [{ query: AllGroups }]
        })
          .then(() => {
            Toast.fire({
              icon: "success",
              title: "הקבוצה נמחקה"
            });
            history.push("/");
          })
          .catch(error => {
            Swal.fire({
              icon: "error",
              title: error,
              toast: true,
              showConfirmButton: true
            });
          })
      : hangeCloseModalAction();
  };
  return (
    <Modal
      className={classes.modal}
      open={modalOpen}
      onClose={() => hangeCloseModalAction()}
      closeAfterTransition
      BackdropComponent={Backdrop}
      BackdropProps={{
        timeout: 500
      }}
    >
      <Grid
        container
        direction="column"
        justify="center"
        alignItems="center"
        className={classes.paper}
      >
        <Grid item>
          <Typography variant="h4" className={classes.title}>
            {"התראת מחיקה"}
          </Typography>
        </Grid>
        <Grid item>
          <Typography variant="body1" className={`${classes.verticalSpacing}`}>
            {`פעולת מחיקת קבוצה כוללת מחיקת כל המידע הקשור לאותה קבוצה כמו ציונים, קשרים וכו'.`}
          </Typography>
        </Grid>
        <Grid item>
          <Typography variant="body1">
            {`אם ברצונכם להמשיך למחיקת הקבוצה הקלידו את  שם הקבוצה : ${currentGroupName}`}
          </Typography>
        </Grid>
        <Grid item>
          {DeleteLoading ? (
            <div
              className={`${classes.verticalSpacing} ${classes.loadingAnimation}`}
            >
              <CircularProgress color="secondary" />
            </div>
          ) : (
            <Fade in={modalOpen}>
              <StyledTextField
                className={`${classes.verticalSpacing}`}
                label="הקלידו את שם הקבוצה"
                onChange={handleGroupNameChange}
                value={GroupName}
              />
            </Fade>
          )}
        </Grid>
        <Grid container direction="row" justify="center" spacing={5}>
          <Grid item>
            <Button
              variant="contained"
              color="secondary"
              className={`${classes.button}`}
              onClick={() => DeleteGroupAction()}
            >
              מחיקה
            </Button>
          </Grid>
          <Grid item>
            <Button
              variant="contained"
              color="secondary"
              className={`${classes.button}`}
              onClick={() => hangeCloseModalAction()}
            >
              ביטול
            </Button>
          </Grid>
        </Grid>
      </Grid>
    </Modal>
  );
};
export default DeleteGroupModal;
