import React, { useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import {
  Modal,
  Backdrop,
  Fade,
  Radio,
  RadioGroup,
  FormControlLabel,
  FormControl,
  FormLabel,
  Button,
  CircularProgress,
  withStyles,
  TextField,
  Typography,
  Grid
} from "@material-ui/core";
import Swal from "sweetalert2";
import UpdateUserAssign from "../../graphql/UpdateUserAssign";
import { useMutation } from "@apollo/react-hooks";
import UsersAssignedToGroupByAPI from "../../graphql/UsersAssignedToGroupByAPI";

const StyledTextField = withStyles({
  root: {
    "& .MuiInputLabel-formControl": {
      left: "unset"
    }
  }
})(TextField);

const useStyles = makeStyles(theme => ({
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    direction: "rtl"
  },
  paper: {
    backgroundColor: theme.palette.background.paper,
    border: "2px solid #f50057",
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
    borderRadius: "8px",
    width: "fit-content"
  },
  title: {
    fontWeight: "bold",
    textAlign: "center"
  },
  form: {
    marginTop: "5%"
  },
  verticalSpacing: {
    marginTop: "7%"
  },
  button: {
    marginTop: "10%",
    marginBottom: "-8%",
    fontSize: "16px",
    width: "20%"
  },
  loadingAnimation: {
    display: "flex",
    justifyContent: "center"
  },
  groupNotSelectedErrorText: {
    marginTop: "10%",
    color: "red"
  }
}));

const TransferModal = ({
  modalOpen,
  setModalopen,
  groupSiblings,
  user,
  currentGroup
}) => {
  const classes = useStyles();
  const [selectedGroup, setSelectedGroup] = useState("");
  const [newStudentNumber, setNewStudentNumber] = useState(
    user.assignments[0].details.student_number
      ? user.assignments[0].details.student_number
      : ""
  );
  const [updateAssign, { loading: updateLoading }] = useMutation(
    UpdateUserAssign
  );
  const handleGroupChange = event => {
    setSelectedGroup(event.target.value);
  };
  const handleNumberChange = event => {
    setNewStudentNumber(event.target.value);
  };
  const transferGroup = () => {
    if (
      selectedGroup &&
      (user.assignments[0].details.student_number
        ? newStudentNumber !== ""
        : true)
    ) {
      selectedGroup !== currentGroup
        ? updateAssign({
            variables: {
              assignID: user.assignments[0].assigns_id,
              groupID: selectedGroup,
              userRoleId: user.assignments[0].user_role_id,
              newDetails: {
                ...user.assignments[0].details,
                ...(user.assignments[0].details.student_number && {
                  student_number: newStudentNumber
                })
              }
            },
            awaitRefetchQueries: true,
            refetchQueries: [
              {
                query: UsersAssignedToGroupByAPI,
                variables: { groupId: currentGroup }
              },
              {
                query: UsersAssignedToGroupByAPI,
                variables: { groupId: selectedGroup }
              }
            ]
          })
            .then(() => {
              Swal.fire({
                icon: "success",
                title: "השינויים נשמרו",
                toast: true,
                showConfirmButton: false,
                timer: 1500
              });
            })
            .catch(error => {
              Swal.fire({
                icon: "error",
                title: error,
                toast: true,
                showConfirmButton: true
              });
            })
        : setModalopen(false);
    }
  };
  return (
    <Modal
      className={classes.modal}
      open={modalOpen}
      onClose={() => {
        setModalopen(false);
        Swal.fire({
          icon: "error",
          title: "הפעולה בוטלה",
          toast: true,
          showConfirmButton: false,
          timer: 1500
        });
      }}
      closeAfterTransition
      BackdropComponent={Backdrop}
      BackdropProps={{
        timeout: 500
      }}
    >
      <Grid
        container
        direction="column"
        justify="center"
        alignItems="center"
        className={classes.paper}
      >
        <Grid item>
          <Typography variant="h4" className={classes.title}>
            {"העברת קבוצה"}
          </Typography>
        </Grid>
        <Grid item>
          <Typography variant="h6" className={classes.verticalSpacing}>
            {"קבוצה נוכחית - " +
              groupSiblings.find(group => group.id.toString() === currentGroup)
                .name}
          </Typography>
        </Grid>
        <Grid item>
          {updateLoading ? (
            <div
              className={`${classes.verticalSpacing} ${classes.loadingAnimation}`}
            >
              <CircularProgress color="secondary" />
            </div>
          ) : (
            <Fade in={modalOpen}>
              <FormControl className={`${classes.form}`}>
                <FormLabel>בחרו לאיזו קבוצה להעביר</FormLabel>
                <RadioGroup
                  row
                  value={selectedGroup}
                  onChange={handleGroupChange}
                >
                  {groupSiblings
                    .filter(({ id }) => JSON.stringify(id) !== currentGroup)
                    .map(group => (
                      <FormControlLabel
                        key={group.id}
                        value={JSON.stringify(group.id)}
                        control={<Radio />}
                        label={group.name}
                      />
                    ))}
                </RadioGroup>
                {user.assignments[0].details.student_number && (
                  <StyledTextField
                    className={classes.verticalSpacing}
                    label="מספר חניך חדש"
                    onChange={handleNumberChange}
                    value={newStudentNumber}
                    error={newStudentNumber === ""}
                    helperText={
                      newStudentNumber === "" ? "הזינו מספר חניך" : ""
                    }
                  />
                )}
              </FormControl>
            </Fade>
          )}
        </Grid>
        {selectedGroup === "" && (
          <Grid item>
            <Typography className={classes.groupNotSelectedErrorText}>
              {"חובה לבחור קבוצה!"}
            </Typography>
          </Grid>
        )}
        <Grid item>
          <Button
            variant="contained"
            color="secondary"
            className={`${classes.button}`}
            onClick={() => transferGroup()}
          >
            שמור
          </Button>
        </Grid>
      </Grid>
    </Modal>
  );
};
export default TransferModal;
