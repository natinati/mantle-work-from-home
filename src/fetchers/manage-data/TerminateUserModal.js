import React, { useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import {
  Modal,
  Backdrop,
  Fade,
  Button,
  CircularProgress,
  TextField,
  Typography,
  withStyles,
  Grid
} from "@material-ui/core";
import Swal from "sweetalert2";
import { useMutation } from "@apollo/react-hooks";
import UsersAssignedToGroupByAPI from "../../graphql/UsersAssignedToGroupByAPI";
import TerminateUser from "../../graphql/TerminateUser";
import { useParams } from "react-router-dom";

const StyledTextField = withStyles({
  root: {
    "& .MuiInputLabel-formControl": {
      left: "unset"
    }
  }
})(TextField);

const useStyles = makeStyles(theme => ({
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    direction: "rtl"
  },
  paper: {
    backgroundColor: theme.palette.background.paper,
    border: "2px solid #f50057",
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
    borderRadius: "8px",
    width: "fit-content"
  },
  title: {
    fontWeight: "bold",
    textAlign: "center"
  },
  verticalSpacing: {
    marginTop: "3%"
  },
  button: {
    marginTop: "10%",
    marginBottom: "-8%",
    fontSize: "16px",
    width: "20%"
  },
  loadingAnimation: {
    display: "flex",
    justifyContent: "center"
  }
}));

const TerminateUserModal = ({ modalOpen, setModalopen, user }) => {
  const classes = useStyles();
  const Toast = Swal.mixin({
    toast: true,
    showConfirmButton: false,
    timer: 1500
  });
  const { groupId } = useParams();
  const [terminationReason, setTerminationReason] = useState("");
  const [terminateUserMutation, { loading: TerminateLoading }] = useMutation(
    TerminateUser
  );
  const handleTerminationReasonChange = event => {
    setTerminationReason(event.target.value);
  };
  const hangeCloseModalAction = () => {
    setTerminationReason("");
    setModalopen(false);
    Toast.fire({
      icon: "error",
      title: "הפעולה בוטלה"
    });
  };
  const TerminateUserAction = () => {
    terminationReason !== ""
      ? terminateUserMutation({
          variables: {
            userRoleId: user.assignments[0].user_role_id,
            reason: terminationReason
          },
          awaitRefetchQueries: true,
          refetchQueries: [
            {
              query: UsersAssignedToGroupByAPI,
              variables: { groupId: groupId }
            }
          ]
        })
          .then(() => {
            Toast.fire({
              icon: "success",
              title: "המשתמש הודח"
            });
          })
          .catch(error => {
            Swal.fire({
              icon: "error",
              title: error,
              toast: true,
              showConfirmButton: true
            });
          })
      : hangeCloseModalAction();
  };
  return (
    <Modal
      className={classes.modal}
      open={modalOpen}
      onClose={() => hangeCloseModalAction()}
      closeAfterTransition
      BackdropComponent={Backdrop}
      BackdropProps={{
        timeout: 500
      }}
    >
      <Grid
        container
        direction="column"
        justify="center"
        alignItems="center"
        className={classes.paper}
      >
        <Grid item>
          <Typography variant="h4" className={classes.title}>
            {"התראת הדחה"}
          </Typography>
        </Grid>
        <Grid item>
          <Typography variant="body1" className={`${classes.verticalSpacing}`}>
            {`פעולה זו תדיח את המשתמש מהקבוצה. על מנת להמשיך עם הפעולה נא לציין את סיבת ההדחה.`}
          </Typography>
        </Grid>
        <Grid item>
          {TerminateLoading ? (
            <div
              className={`${classes.verticalSpacing} ${classes.loadingAnimation}`}
            >
              <CircularProgress color="secondary" />
            </div>
          ) : (
            <Fade in={modalOpen}>
              <StyledTextField
                className={`${classes.verticalSpacing}`}
                label="הקלידו את סיבת ההדחה"
                onChange={handleTerminationReasonChange}
                value={terminationReason}
              />
            </Fade>
          )}
        </Grid>
        <Grid container direction="row" justify="center" spacing={5}>
          <Grid item>
            <Button
              variant="contained"
              color="secondary"
              className={`${classes.button}`}
              onClick={() => TerminateUserAction()}
            >
              הדחה
            </Button>
          </Grid>
          <Grid item>
            <Button
              variant="contained"
              color="secondary"
              className={`${classes.button}`}
              onClick={() => hangeCloseModalAction()}
            >
              ביטול
            </Button>
          </Grid>
        </Grid>
      </Grid>
    </Modal>
  );
};
export default TerminateUserModal;
