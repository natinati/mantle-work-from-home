import React, { useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import {
  Modal,
  Backdrop,
  Fade,
  Radio,
  RadioGroup,
  FormControlLabel,
  FormControl,
  FormLabel,
  Button,
  CircularProgress,
  TextField,
  Typography,
  withStyles,
  Grid
} from "@material-ui/core";
import Swal from "sweetalert2";
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker
} from "@material-ui/pickers";
import DateFnsUtils from "@date-io/date-fns";
import { useMutation } from "@apollo/react-hooks";
import AllGroups from "../../graphql/AllGroups";
import InsertGroup from "../../graphql/InsertGroup";
import InsertCourse from "../../graphql/InsertCourse";
import { groupTypeIDS } from "../../constants";

const StyledTextField = withStyles({
  root: {
    "& .MuiInputLabel-formControl": {
      left: "unset"
    }
  }
})(TextField);

const StyledDateField = withStyles({
  root: {
    "& .MuiInputLabel-formControl": {
      left: "unset"
    }
  }
})(KeyboardDatePicker);

const useStyles = makeStyles(theme => ({
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    direction: "rtl"
  },
  paper: {
    backgroundColor: theme.palette.background.paper,
    border: "2px solid #f50057",
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
    borderRadius: "8px",
    width: "fit-content"
  },
  title: {
    fontWeight: "bold",
    textAlign: "center"
  },
  form: {
    marginTop: "5%"
  },
  verticalSpacing: {
    marginTop: "7%"
  },
  button: {
    marginTop: "10%",
    marginBottom: "-8%",
    fontSize: "16px",
    width: "20%"
  },
  loadingAnimation: {
    display: "flex",
    justifyContent: "center"
  },
  groupNotSelectedErrorText: {
    marginTop: "10%",
    color: "red"
  }
}));

const NewGroupModal = ({
  modalOpen,
  setModalopen,
  currentGroup,
  currentGroupType
}) => {
  const classes = useStyles();
  const Toast = Swal.mixin({
    toast: true,
    showConfirmButton: false,
    timer: 1500
  });
  const inputOptions = {
    [groupTypeIDS.MEGAMA]: { 2: "מחזור", 4: "קורס" },
    [groupTypeIDS.CYCLE]: { 4: "קורס" },
    [groupTypeIDS.COURSE]: { 3: "מחלקה", 1: "צוות" },
    [groupTypeIDS.MADOR]: { 5: "מגמה" },
    [groupTypeIDS.YEHIDA]: { 7: "מדור" }
  };
  const [selectedGroupType, setSelectedGroupType] = useState("");
  const [newGroupName, setNewGroupName] = useState("");
  const [selectedCourseStartDate, setSelectedCourseStartDate] = useState(
    new Date()
  );
  const [selectedCourseEndDate, setSelectedCourseEndDate] = useState(
    new Date()
  );
  const [insertGroupMutation, { loading: insertGroupLoading }] = useMutation(
    InsertGroup
  );
  const [insertCourseMutation, { loading: insertCourseLoading }] = useMutation(
    InsertCourse
  );
  const handleCourseStartDateChange = date => {
    setSelectedCourseStartDate(date);
  };
  const handleCourseEndDateChange = date => {
    setSelectedCourseEndDate(date);
  };
  const handleGroupTypeChange = event => {
    setSelectedGroupType(event.target.value);
  };
  const handleGroupNameChange = event => {
    setNewGroupName(event.target.value);
  };
  const insertGroupAction = () => {
    if (selectedGroupType !== "" && newGroupName !== "") {
      if (selectedGroupType === groupTypeIDS.COURSE.toString()) {
        if (selectedCourseStartDate && selectedCourseEndDate) {
          insertCourseMutation({
            variables: {
              groupTypeId: selectedGroupType,
              groupName: newGroupName,
              groupParentId: currentGroup,
              startDate: selectedCourseStartDate,
              endDate: selectedCourseEndDate
            },
            awaitRefetchQueries: true,
            refetchQueries: [{ query: AllGroups }]
          })
            .then(() => {
              Toast.fire({
                icon: "success",
                title: "הקבוצה נוספה"
              });
            })
            .catch(error => {
              Swal.fire({
                icon: "error",
                title: error,
                toast: true,
                showConfirmButton: true
              });
            });
        }
      } else {
        insertGroupMutation({
          variables: {
            groupTypeId: selectedGroupType,
            groupName: newGroupName,
            groupParentId: currentGroup
          },
          awaitRefetchQueries: true,
          refetchQueries: [{ query: AllGroups }]
        })
          .then(() => {
            setModalopen(false);
            Toast.fire({
              icon: "success",
              title: "הקבוצה נוספה"
            });
          })
          .catch(error => {
            setModalopen(false);
            Swal.fire({
              icon: "error",
              title: error,
              toast: true,
              showConfirmButton: true
            });
          });
      }
    }
  };
  return (
    <Modal
      className={classes.modal}
      open={modalOpen}
      onClose={() => {
        setModalopen(false);
        Toast.fire({
          icon: "error",
          title: "הפעולה בוטלה"
        });
      }}
      closeAfterTransition
      BackdropComponent={Backdrop}
      BackdropProps={{
        timeout: 500
      }}
    >
      <Grid
        container
        direction="column"
        justify="center"
        alignItems="center"
        className={classes.paper}
      >
        <Grid item>
          <Typography variant="h4" className={classes.title}>
            {"הוספת תת קבוצה"}
          </Typography>
        </Grid>
        <Grid item>
          {insertGroupLoading || insertCourseLoading ? (
            <div
              className={`${classes.verticalSpacing} ${classes.loadingAnimation}`}
            >
              <CircularProgress color="secondary" />
            </div>
          ) : (
            <Fade in={modalOpen}>
              <FormControl className={`${classes.form}`}>
                <FormLabel>מהו סוג הקבוצה?</FormLabel>
                <RadioGroup
                  row
                  value={selectedGroupType}
                  onChange={handleGroupTypeChange}
                >
                  {Object.entries(inputOptions[currentGroupType]).map(
                    groupType => (
                      <FormControlLabel
                        key={groupType[0]}
                        value={groupType[0]}
                        control={<Radio />}
                        label={groupType[1]}
                      />
                    )
                  )}
                </RadioGroup>
                <StyledTextField
                  label="מה שם הקבוצה?"
                  onChange={handleGroupNameChange}
                  value={newGroupName}
                  error={newGroupName === ""}
                  helperText={newGroupName === "" ? "הזינו שם לקבוצה" : ""}
                />
                {selectedGroupType === groupTypeIDS.COURSE.toString() && (
                  <Grid container item spacing={2} className={classes.form}>
                    <Grid item>
                      <MuiPickersUtilsProvider utils={DateFnsUtils}>
                        <StyledDateField
                          disableToolbar
                          variant="inline"
                          format="dd/MM/yyyy"
                          margin="normal"
                          label="מה תאריך תחילת הקורס?"
                          invalidDateMessage="חובה להזין תאריך!"
                          value={selectedCourseStartDate}
                          onChange={handleCourseStartDateChange}
                          InputLabelProps={{
                            shrink: true
                          }}
                        />
                      </MuiPickersUtilsProvider>
                    </Grid>
                    <Grid item>
                      <MuiPickersUtilsProvider utils={DateFnsUtils}>
                        <StyledDateField
                          disableToolbar
                          variant="inline"
                          format="dd/MM/yyyy"
                          margin="normal"
                          label="מה תאריך סיום הקורס?"
                          invalidDateMessage="חובה להזין תאריך!"
                          value={selectedCourseEndDate}
                          onChange={handleCourseEndDateChange}
                          InputLabelProps={{
                            shrink: true
                          }}
                        />
                      </MuiPickersUtilsProvider>{" "}
                    </Grid>
                  </Grid>
                )}
              </FormControl>
            </Fade>
          )}
        </Grid>
        {selectedGroupType === "" && (
          <Grid item>
            <Typography className={classes.groupNotSelectedErrorText}>
              {"חובה לבחור סוג קבוצה!"}
            </Typography>
          </Grid>
        )}
        {selectedGroupType === groupTypeIDS.COURSE.toString() &&
          (!selectedCourseStartDate || !selectedCourseEndDate) && (
            <Grid item>
              <Typography className={classes.groupNotSelectedErrorText}>
                {"חובה להזין את תאריכי הקורס!"}
              </Typography>
            </Grid>
          )}
        <Grid item>
          <Button
            variant="contained"
            color="secondary"
            className={`${classes.button}`}
            onClick={() => insertGroupAction()}
          >
            שמור
          </Button>
        </Grid>
      </Grid>
    </Modal>
  );
};
export default NewGroupModal;
