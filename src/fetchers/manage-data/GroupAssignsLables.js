import React, { useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import { Grid, IconButton, Tooltip } from "@material-ui/core";
import { useParams } from "react-router-dom";
import RemoveIcon from "@material-ui/icons/Remove";
import ForwardIcon from "@material-ui/icons/Forward";
import DeleteIcon from "@material-ui/icons/Delete";
import DeleteUserModal from "./DeleteUserModal";
import TerminateUserModal from "./TerminateUserModal";

const useStyles = makeStyles(() => ({
  title: {
    fontSize: 14
  },
  Icons: {
    padding: 0
  },
  Direction: {
    direction: "rtl"
  },
  currentGroupAssignBold: {
    fontWeight: "bold",
    textDecoration: "underline"
  }
}));

const GroupLable = ({ setModalOpen, groupSiblings, user }) => {
  const classes = useStyles();
  const [DeleteUserModalOpen, setDeleteUserModalOpen] = useState(false);
  const [TerminateUserModalOpen, setTerminateUserModalOpen] = useState(false);
  const { groupId } = useParams();
  return (
    <Grid container>
      <DeleteUserModal
        modalOpen={DeleteUserModalOpen}
        setModalopen={setDeleteUserModalOpen}
        user={user}
      />
      <TerminateUserModal
        modalOpen={TerminateUserModalOpen}
        setModalopen={setTerminateUserModalOpen}
        user={user}
      />
      <Grid container item direction="row" spacing={3}>
        {groupSiblings.filter(({ id }) => JSON.stringify(id) !== groupId)
          .length > 0 && (
          <Grid item xs={3}>
            <Tooltip title="העברת משתמש">
              <IconButton
                className={classes.Icons}
                onClick={() => setModalOpen(true)}
              >
                <ForwardIcon style={{ color: "grey", fontSize: "70%" }} />
              </IconButton>
            </Tooltip>
          </Grid>
        )}
        {!user.assignments[0].if_terminated_reason && (
          <Grid item xs={3}>
            <Tooltip title="הדחה מהקבוצה">
              <IconButton
                className={classes.Icons}
                onClick={() => {
                  setTerminateUserModalOpen(true);
                }}
              >
                <RemoveIcon style={{ fontSize: "70%" }} />
              </IconButton>
            </Tooltip>
          </Grid>
        )}
        <Grid item xs={3}>
          <Tooltip title="הסרה מהקבוצה">
            <IconButton
              className={classes.Icons}
              onClick={() => {
                setDeleteUserModalOpen(true);
              }}
            >
              <DeleteIcon style={{ color: "red", fontSize: "70%" }} />
            </IconButton>
          </Tooltip>
        </Grid>
      </Grid>
    </Grid>
  );
};

export default GroupLable;
