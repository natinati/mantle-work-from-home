import React, { useState, useEffect } from "react";
import {
  GridList,
  makeStyles,
  TextField,
  Grid,
  CircularProgress,
  List,
  ListItem,
  ListItemText,
  Menu,
  MenuItem,
  Button,
  Tooltip,
  withStyles,
  Breadcrumbs,
  Typography,
  Checkbox,
  FormControlLabel
} from "@material-ui/core";
import { useQuery } from "@apollo/react-hooks";
import styled from "styled-components";
import UserCard from "./UserCard";
import GroupSiblingsByAPI from "../../graphql/GroupSiblingsByAPI";
import GroupChildsByAPI from "../../graphql/GroupChildsByAPI";
import UsersAssignedToGroupByAPI from "../../graphql/UsersAssignedToGroupByAPI";
import GroupTypeById from "../../graphql/GroupTypeById";
import GroupPathByAncestorsView from "../../graphql/GroupPathByAncestorsView";
import * as R from "ramda";
import { useParams, Link } from "react-router-dom";
import { groupTypeIDS } from "../../constants";
import NewGroupModal from "./NewGroupModal";
import UpdateGroupNameModal from "./UpdateGroupNameModal";
import DeleteGroupModal from "./DeleteGroupModal";

const StyledTextField = withStyles({
  root: {
    "& .MuiInputLabel-formControl": {
      left: "unset"
    }
  }
})(TextField);

const BreadcrumbsLink = styled(Link)`
  text-decoration: none;
  color: black;
  ${({ shouldbebold }) =>
    shouldbebold &&
    `
  font-weight: bold;
  text-decoration: underline;
`}
`;

const useStyles = makeStyles(theme => ({
  gridList: {
    height: "75vh",
    direction: "rtl",
    marginTop: "1% !important"
  },
  container: {
    marginTop: theme.spacing(4)
  },
  searchAndFilter: {
    direction: "rtl",
    marginTop: "1%",
    margin: "8"
  },
  Direction: {
    direction: "rtl"
  },
  disabledButton: {
    opacity: 0.5
  },
  buttonTextSize: {
    fontSize: "16px"
  }
}));

const UsersAndGroupsPage = () => {
  const [roleFilter, setRoleFilter] = useState([]);
  const [nameAndSoldierIdFilter, setNameAndSoldierIdFilter] = useState([]);
  const [terminatedUsersFilter, setTerminatedUsersFilter] = useState([
    user => user.assignments.filter(assign => !assign.if_terminated_reason)[0]
  ]);
  const [anchorEl, setAnchorEl] = useState(null);
  const [selectedRoleIndex, setSelectedRoleIndex] = useState(0);
  const [
    showTerminatedUsersCheckbox,
    setShowTerminatedUsersCheckbox
  ] = useState(false);
  const [NewGroupModalOpen, setNewGroupModalOpen] = useState(false);
  const [UpdateGroupNameModalOpen, setUpdateGroupNameModalOpen] = useState(
    false
  );
  const [DeleteGroupModalOpen, setDeleteGroupModalOpen] = useState(false);
  const classes = useStyles();
  const { groupId } = useParams();
  useEffect(() => {
    setSelectedRoleIndex(0);
    setShowTerminatedUsersCheckbox(false);
    setNameAndSoldierIdFilter([]);
    setRoleFilter([]);
    setTerminatedUsersFilter([
      user => user.assignments.filter(assign => !assign.if_terminated_reason)[0]
    ]);
  }, [groupId]);
  const usersAssignedToSelectedGroupQuery = useQuery(
    UsersAssignedToGroupByAPI,
    {
      variables: { groupId: groupId }
    }
  );
  const groupTypeQuery = useQuery(GroupTypeById, {
    variables: { groupId: groupId }
  });
  const GroupSiblingsQuery = useQuery(GroupSiblingsByAPI, {
    variables: { groupId: groupId },
    fetchPolicy: "network-only"
  });
  const GroupChildsQuery = useQuery(GroupChildsByAPI, {
    variables: { groupId: groupId }
  });
  const GroupPathByAncestorsViewQuery = useQuery(GroupPathByAncestorsView, {
    variables: { groupId: groupId }
  });
  const roleOptions = [
    "הכל",
    "מדריך",
    "חניך",
    'צפ"ה',
    'מק"ס',
    'מ"מ',
    'רמ"ד',
    "מפקד יחידה"
  ];

  const handleClickRoleListItem = event => {
    setAnchorEl(event.currentTarget);
  };

  const handleRoleMenuItemClick = index => {
    setSelectedRoleIndex(index);
    roleOptions[index] === "הכל"
      ? setRoleFilter([])
      : setRoleFilter([
          user =>
            user.assignments.filter(
              assign => assign.group_id === parseInt(groupId)
            )[0]
              ? user.assignments.filter(
                  assign => assign.group_id === parseInt(groupId)
                )[0].role_name === roleOptions[index]
              : []
        ]);
    setAnchorEl(null);
  };

  const handleRoleMenuClose = () => {
    setAnchorEl(null);
  };

  const handleNameAndSoldierIdFilterTextOnChange = event => {
    const searchText = event.target.value;
    searchText === ""
      ? setNameAndSoldierIdFilter([])
      : setNameAndSoldierIdFilter([
          user =>
            isNaN(searchText)
              ? user.first_name.concat(" ", user.last_name).includes(searchText)
              : user.soldier_id.toString().startsWith(searchText)
        ]);
  };

  const handleShowTerminatedUsersCheckboxChange = event => {
    const checkboxState = event.target.checked;
    setShowTerminatedUsersCheckbox(checkboxState);
    checkboxState
      ? setTerminatedUsersFilter([
          user =>
            user.assignments.filter(assign => assign.if_terminated_reason)[0]
        ])
      : setTerminatedUsersFilter([
          user =>
            user.assignments.filter(assign => !assign.if_terminated_reason)[0]
        ]);
  };

  return usersAssignedToSelectedGroupQuery.loading ||
    groupTypeQuery.loading ||
    GroupSiblingsQuery.loading ||
    GroupPathByAncestorsViewQuery.loading ||
    GroupChildsQuery.loading ? (
    <div className={classes.container}>
      <Grid container direction="row" justify="center" alignItems="center">
        <CircularProgress color="secondary" />
      </Grid>
    </div>
  ) : (
    <div className={classes.container}>
      {![groupTypeIDS.TEAM, groupTypeIDS.DEPARTMENT].includes(
        groupTypeQuery.data.core_groups[0].group_type.id
      ) && (
        <NewGroupModal
          modalOpen={NewGroupModalOpen}
          setModalopen={setNewGroupModalOpen}
          currentGroup={groupId}
          currentGroupType={groupTypeQuery.data.core_groups[0].group_type.id}
        />
      )}
      <UpdateGroupNameModal
        modalOpen={UpdateGroupNameModalOpen}
        setModalopen={setUpdateGroupNameModalOpen}
        currentGroup={groupId}
      />
      <DeleteGroupModal
        modalOpen={DeleteGroupModalOpen}
        setModalopen={setDeleteGroupModalOpen}
        currentGroup={groupId}
        currentGroupName={groupTypeQuery.data.core_groups[0].name}
      />
      <Grid container item>
        <Breadcrumbs separator=">">
          {GroupPathByAncestorsViewQuery.data.path
            .sort((a, b) => (a.depth < b.depth ? 1 : -1))
            .map(group => (
              <BreadcrumbsLink
                to={{
                  pathname: `/group/${group.ancestor.id}/manage-data`
                }}
                shouldbebold={groupId === group.ancestor.id.toString() ? 1 : 0}
                key={group.ancestor.id}
              >
                {group.ancestor.name}
              </BreadcrumbsLink>
            ))}
        </Breadcrumbs>
      </Grid>
      <Grid
        container
        direction="row"
        alignItems="center"
        className={classes.searchAndFilter}
      >
        <Grid item>
          <StyledTextField
            label="חיפוש"
            style={{ margin: 8 }}
            placeholder="הקלד שם/מספר אישי"
            type="search"
            onChange={handleNameAndSoldierIdFilterTextOnChange}
          />
        </Grid>
        <Grid item style={{ marginRight: "1%" }}>
          <List>
            <ListItem button onClick={handleClickRoleListItem}>
              <ListItemText
                primary="תפקיד"
                secondary={roleOptions[selectedRoleIndex]}
              />
            </ListItem>
          </List>
          <Menu
            anchorEl={anchorEl}
            keepMounted
            open={Boolean(anchorEl)}
            onClose={handleRoleMenuClose}
            style={{ direction: "rtl" }}
          >
            {roleOptions.map((option, index) => (
              <MenuItem
                key={option}
                selected={index === selectedRoleIndex}
                onClick={() => handleRoleMenuItemClick(index)}
              >
                {option}
              </MenuItem>
            ))}
          </Menu>
        </Grid>
        {![groupTypeIDS.TEAM, groupTypeIDS.DEPARTMENT].includes(
          groupTypeQuery.data.core_groups[0].group_type.id
        ) && (
          <Grid item style={{ marginRight: "3%" }}>
            <Button
              size="medium"
              variant="outlined"
              onClick={async () => {
                setNewGroupModalOpen(true);
              }}
            >
              הוספת תת קבוצה
            </Button>
          </Grid>
        )}
        <Grid item style={{ marginRight: "3%" }}>
          <Button
            size="medium"
            variant="outlined"
            onClick={async () => {
              setUpdateGroupNameModalOpen(true);
            }}
          >
            עריכת שם הקבוצה
          </Button>
        </Grid>
        <Grid item style={{ marginRight: "3%" }}>
          <Tooltip
            className={classes.Direction}
            title={
              usersAssignedToSelectedGroupQuery.data.api_all_users.length
                ? ".לא ניתן למחוק קבוצה זו כיוון שיש חניכים ו/או סגלים השייכים אליה"
                : GroupChildsQuery.data.api_all_groups[0].children.length
                ? ".לא ניתן למחוק קבוצה זו כיוון שיש לה קבוצות בת"
                : ""
            }
          >
            <Button
              color="secondary"
              size="medium"
              variant="outlined"
              disableRipple={
                usersAssignedToSelectedGroupQuery.data.api_all_users.length >
                  0 ||
                GroupChildsQuery.data.api_all_groups[0].children.length > 0
              }
              className={
                usersAssignedToSelectedGroupQuery.data.api_all_users.length ||
                GroupChildsQuery.data.api_all_groups[0].children.length
                  ? classes.disabledButton
                  : null
              }
              onClick={
                usersAssignedToSelectedGroupQuery.data.api_all_users.length ||
                GroupChildsQuery.data.api_all_groups[0].children.length
                  ? null
                  : async () => {
                      setDeleteGroupModalOpen(true);
                    }
              }
            >
              מחיקת קבוצה
            </Button>
          </Tooltip>
        </Grid>
        <Grid container item>
          <FormControlLabel
            control={
              <Checkbox
                checked={showTerminatedUsersCheckbox}
                onChange={handleShowTerminatedUsersCheckboxChange}
              />
            }
            label={"הצגת משתמשים מודחים"}
          />
        </Grid>
      </Grid>
      <GridList className={classes.gridList}>
        {usersAssignedToSelectedGroupQuery.data.api_all_users.length ? (
          R.filter(
            R.allPass(
              roleFilter.concat(nameAndSoldierIdFilter, terminatedUsersFilter)
            )
          )(usersAssignedToSelectedGroupQuery.data.api_all_users).map(user => (
            <UserCard
              key={user.soldier_id}
              user={user}
              groupSiblings={
                GroupSiblingsQuery.data.api_all_groups[0].parent.children
              }
            />
          ))
        ) : (
          <Grid item>
            <Typography variant="h5">הקבוצה ריקה.</Typography>
          </Grid>
        )}
      </GridList>
    </div>
  );
};
export default UsersAndGroupsPage;
