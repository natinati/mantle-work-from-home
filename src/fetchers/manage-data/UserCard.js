import React, { useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import GroupLable from "./GroupAssignsLables";
import {
  Card,
  CardContent,
  Typography,
  GridListTile,
  Grid
} from "@material-ui/core";
import { useParams } from "react-router-dom";
import TransferModal from "./TransferModal";

const useStyles = makeStyles(theme => ({
  title: {
    fontSize: 14
  },
  userCard: {
    margin: theme.spacing(1),
    minHeight: "13vh",
    height: "fit-content",
    width: "fit-content"
  },
  Direction: {
    direction: "rtl"
  },
  listRoot: {
    overflow: "auto",
    maxHeight: "10vh"
  },
  expandCardButton: {
    "&:hover": {
      backgroundColor: "transparent"
    }
  },
  scrollOnHover: {
    overflow: "hidden",
    "&:hover": {
      overflowY: "auto"
    }
  }
}));

const UserCard = ({ user, groupSiblings }) => {
  const { groupId } = useParams();
  const classes = useStyles();
  const [modalOpen, setModalopen] = useState(false);

  return (
    <div>
      <TransferModal
        modalOpen={modalOpen}
        setModalopen={setModalopen}
        groupSiblings={groupSiblings}
        user={user}
        currentGroup={groupId}
      />
      <GridListTile key={user.soldier_id}>
        <Card className={classes.userCard} variant="outlined">
          <CardContent>
            <Grid container direction="row">
              <Grid container item direction="column" md={6}>
                <Typography
                  className={classes.title}
                  color="textSecondary"
                  gutterBottom
                >
                  {`${user.first_name} ${user.last_name}`}
                </Typography>
                <Typography
                  className={classes.title}
                  color="textSecondary"
                  gutterBottom
                >
                  {`${user.soldier_id}`}
                </Typography>
                <Typography
                  className={classes.title}
                  color="textSecondary"
                  gutterBottom
                >
                  {`${
                    user.assignments.find(
                      assign => assign.group_id === parseInt(groupId)
                    ).role_name
                  }`}
                </Typography>
                {user.assignments[0].details.student_number && (
                  <Typography
                    className={classes.title}
                    color="textSecondary"
                    gutterBottom
                  >
                    {user.assignments[0].details.student_number}
                  </Typography>
                )}
              </Grid>
              <Grid container item direction="column" md={6}>
                <List
                  className={`${classes.listRoot} ${classes.scrollOnHover}`}
                >
                  {user.assignments.map(assign => (
                    <ListItem key={assign.assigns_id}>
                      <GroupLable
                        setModalOpen={setModalopen}
                        groupSiblings={groupSiblings}
                        user={user}
                      />
                    </ListItem>
                  ))}
                </List>
              </Grid>
            </Grid>
          </CardContent>
        </Card>
      </GridListTile>
    </div>
  );
};

export default UserCard;
