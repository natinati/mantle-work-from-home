import React from "react";
import Template from "./components/Template";
import Router from "./Router";
import { BrowserRouter } from "react-router-dom";
import { ApolloProvider } from "@apollo/react-hooks";
import client from "./apolloClient";
import {
  withStyles,
  MuiThemeProvider,
  createMuiTheme
} from "@material-ui/core/styles";

const THEME = createMuiTheme({
  typography: {
    fontFamily: `"Heebo", sans-serif`
  }
});

const GlobalCss = withStyles({
  "@global": {
    "*::-webkit-scrollbar": {
      width: "0.5em"
    },
    "*::-webkit-scrollbar-track": {
      boxShadow: "inset 0 0 6px rgba(0,0,0,0.00)",
      webkitBoxShadow: "inset 0 0 6px rgba(0,0,0,0.00)"
    },
    "*::-webkit-scrollbar-thumb": {
      backgroundColor: "rgba(245,0,87,0.7)",
      outline: "1px solid slategrey"
    }
  }
})(() => null);

const App = () => {
  return (
    <div className="App">
      <ApolloProvider client={client}>
        <MuiThemeProvider theme={THEME}>
          <BrowserRouter>
            <GlobalCss />
            <Template>
              <Router />
            </Template>
          </BrowserRouter>
        </MuiThemeProvider>
      </ApolloProvider>
    </div>
  );
};

export default App;
