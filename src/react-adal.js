import { AuthenticationContext, adalFetch, withAdalLogin } from "react-adal";

export const adalConfig = {
  tenant: window["BSMCH_CONFIG"].REACT_APP_TENANT_ID,
  clientId: window["BSMCH_CONFIG"].REACT_APP_CLIENT_ID,
  redirectUri: window["BSMCH_CONFIG"].REACT_APP_REDIRECT_URI,
  cacheLocation: "localStorage"
};

export const authContext = new AuthenticationContext(adalConfig);

export const adalApiFetch = (fetch, url, options) =>
  adalFetch(authContext, adalConfig.redirectUri, fetch, url, options);

export const withAdalLoginApi = withAdalLogin(
  authContext,
  adalConfig.redirectUri
);
