import React from "react";
import { Switch, Route } from "react-router-dom";
import BulkInsertPage from "./fetchers/bulk-insert/BulkInsertPage";
import UsersAndGroupsPage from "./fetchers/manage-data/UsersAndGroupsPage";
import HomePage from "./components/HomePage";
import ErrorPage from "./components/ErrorPage";

const Router = () => (
  <Switch>
    <Route path="/" component={HomePage} exact></Route>
    <Route path="/group/:groupId/manage-data">
      <UsersAndGroupsPage />
    </Route>
    <Route path="/group/:groupId/bulk-insert">
      <BulkInsertPage />
    </Route>
    <Route path="/error">
      <ErrorPage />
    </Route>
  </Switch>
);

export default Router;
