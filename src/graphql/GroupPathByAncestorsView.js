import gql from "graphql-tag";

const GroupPathByAncestorsView = gql`
  query ($groupId: Int!) {
    path: core_group_ancestors(where: {group_id: {_eq: $groupId}}) {
      ancestor {
        id
        name
      }
    depth
  }
}
`;

export default GroupPathByAncestorsView;
