import gql from "graphql-tag";

const InsertGroup = gql`
  mutation($groupName: String!, $groupTypeId: Int!, $groupParentId: Int!) {
    insert_core_groups(
      objects: {
        name: $groupName
        group_type_id: $groupTypeId
        edgesByChild: { data: { parent: $groupParentId } }
      }
    ) {
      affected_rows
    }
  }
`;

export default InsertGroup;
