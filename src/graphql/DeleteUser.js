import gql from "graphql-tag";

const DeleteUser = gql`
  mutation DeleteUser($userId: Int!) {
    delete_core_users(where: { id: { _eq: $userId } }) {
      affected_rows
    }
  }
`;

export default DeleteUser;
