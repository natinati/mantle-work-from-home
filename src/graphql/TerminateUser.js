import gql from "graphql-tag";

const TerminateUser = gql`
  mutation($userRoleId: Int!, $reason: String!) {
    insert_core_users_roles_termination(
      objects: { user_role_id: $userRoleId, reason: $reason }
    ) {
      affected_rows
    }
  }
`;

export default TerminateUser;
