import gql from "graphql-tag";

const InsertCourse = gql`
  mutation(
    $groupName: String!
    $groupTypeId: Int!
    $groupParentId: Int!
    $startDate: date!
    $endDate: date!
  ) {
    insert_core_groups(
      objects: {
        name: $groupName
        group_type_id: $groupTypeId
        edgesByChild: { data: { parent: $groupParentId } }
        groups_periods: { data: { start_date: $startDate, end_date: $endDate } }
      }
    ) {
      affected_rows
    }
  }
`;

export default InsertCourse;
