import gql from "graphql-tag";

const UsersAssignedToGroupByAPI = gql`
  query($groupId: Int!) {
    api_all_users(where: { assignments: { group_id: { _eq: $groupId } } }) {
      user_id
      first_name
      last_name
      soldier_id
      assignments(where: { group_id: { _eq: $groupId } }) {
        assigns_id
        group_id
        group_name
        group_type_id
        group_type_name
        role_id
        role_name
        user_role_id
        details
        if_terminated_reason
      }
    }
  }
`;

export default UsersAssignedToGroupByAPI;
