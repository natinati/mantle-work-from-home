export const groupTypeIDS = {
  TEAM: 1,
  CYCLE: 2,
  DEPARTMENT: 3,
  COURSE: 4,
  MEGAMA: 5,
  MADOR: 7,
  YEHIDA: 8
};
